#!/bin/bash

# check sudo privileges

user=$(whoami)
if [ $user != "root" ]; then
	echo "Permission denied, are you root?">&2
	exit
fi


# create nthuoj table

echo "Creating Database">&2
read -p "Please enter your Mysql user: " sqlUser
read -s -p "Password for $sqlUser: " sqlPasswd
mysql -u $sqlUser -p$sqlPasswd < createTableSqls.sql
echo ""
if [ $? != 0 ]; then
	echo "Database creation failed, please check your password">&2
	exit
fi
echo "Database Created , default admin of OJ username/password is boss/boss ">&2


#create nthuoj folder

dirToInstall=/etc

echo "Install nthuoj folder at $dirToInstall/nthuoj">&2
rm -rf $dirToInstall/nthuoj
mkdir $dirToInstall/nthuoj
mkdir $dirToInstall/nthuoj/mountServer
mkdir $dirToInstall/nthuoj/mountServer/source
mkdir $dirToInstall/nthuoj/mountServer/speJudge
mkdir $dirToInstall/nthuoj/mountServer/testdata
chown -R www-data:www-data $dirToInstall/nthuoj/mountServer


# create nthuoj.ini

iniFile=$dirToInstall/nthuoj/nthuoj.ini

echo "[database]" >> $iniFile
echo ";Default MYSQL user name" >> $iniFile
echo "username = $sqlUser" >> $iniFile
echo ";Default MYSQL password" >> $iniFile
echo "password = $sqlPasswd" >> $iniFile
echo ";Database IP" >> $iniFile
echo "ip = 127.0.0.1" >> $iniFile


# create symbolic link for web page

cd $(dirname $0)
echo $PWD
webRoot=/var/www/nthuoj

echo "Web root: $webRoot">&2
if [ ! -d $webRoot ]; then
	mkdir $webRoot
fi
ln -sf $PWD/www/* $webRoot
ln -sf /usr/share/phpmyadmin /var/www


# install fckeditor

tar zxf $PWD/FCKeditor_2.6.11.tar.gz fckeditor
chown -R root:root fckeditor
mv fckeditor $webRoot/fckeditor


# change template_c authority

chmod 777 www/templates_c


# finish installing frontend

echo "setup frontend finished">&2

