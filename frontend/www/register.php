<?php
/****************************
register.php
This page is used for user registration.
POST parameter 'btnSubmit' act as sign for add new user.
POST parameter 'uid', 'password', 'email', 'nuckname' are used for new user account information.
*****************************/
	session_start();
	$errors = array();
	$message = "";
    include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    $tpl = new Handler("Register", "register.tpl");

	if(isset($_POST["btnSubmit"])) {
        $tpl->assign("uid", $_POST["uid"]);
        $tpl->assign("email", $_POST["email"]);
        $tpl->assign("nickname", $_POST["nickname"]);
		require_once("validation.php");
		$rules = array();
	
		/* require all */
		$rules[] = "required,uid,User ID is required.";
		$rules[] = "required,password,Password is required.";
		$rules[] = "same_as,password,confirm_password,Please ensure the passwords you enter are the same.";
		$rules[] = "required,email,Email is required.";
		$rules[] = "required,nickname,Nickname is required.";
		$rules[] = "required,skey,Secret Key is required.";
		

		$rules[] = "length=3-12,uid,User ID must have length of 3-12.";
		$rules[] = "length=4-12,password,Password must have length of 4-12.";
		$rules[] = "length<=12,nickname,Nickname must have length not greater than 12.";
		
		$rules[] = "is_alpha,uid,User ID must only consist of only (0-9 a-Z).";
		$rules[] = "is_alpha,password,Password must only consist of only (0-9 a-Z).";

		$rules[] = "valid_email,email,Email is invalid.";
		$errors = validateFields($_POST, $rules);
		
		if($_POST["skey"] != "TPC")
            $errors[] = "Wrong secret key.";
		
		if(!empty($errors))
			$message = $errors[0];
		else {
            $con = get_database_object();
			$query = "SELECT id FROM users WHERE id = '".$_POST["uid"]."'";
			$result = mysql_query($query) or die('Query failed.'.mysql_error());
			if(mysql_num_rows($result) != 0)
				$message = "User ID already used. Please choose a different ID.";
			else {
				$query = "INSERT INTO users (id,password,email,nickname,user_level) 
                          VALUES ('".$_POST["uid"]."',PASSWORD('".$_POST['password']."'),'".$_POST['email']."','".$_POST['nickname']."',0)";
				mysql_query($query) or die('Query failed.'.mysql_error());
			}
			mysql_close($con);
		}
	
		if($message=='')
			header("Location: login.php");
    }
    $tpl->assign("msg", $message);
    $tpl->display("base.html");
?>
