<?php
/*****************************************
delete_testcase.php
Namely, this delete a testcase.
Check GET parameter 'tid' to decide which to delete.
******************************************/

    session_start();
	function write_log($str){
		echo "<script>console.log('$str');</script>";
	}
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    include_once("validation.php");
	if( !check_admin() )
        die("You have no judge permission");
	 if(!isset($_GET["tid"]))
		die('Testcase ID does not exist');
	
    $tid = $_GET['tid'];
    if(!ctype_digit($tid))
		die('Testcase ID does not exist');
	$con = get_database_object();
	$query = "SELECT tid FROM testcases where tid =".$tid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	if(mysql_num_rows($result)==0)
		die('Testcase ID does not exist');
	$message = '';    
    $errors = array();
	$rs = array();
	$all = array();
	$query =  "DELETE from testcases where tid = ".$tid;
	$res = mysql_query($query) or die($query."Query failed".mysql_error());
	echo "Delete Succeed";
?>
	