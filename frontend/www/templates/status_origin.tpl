<{if $findUser eq 1 }>
<form method="post" name="findUser">
  User ID: <input name="uid" type="text" size=12 maxlength=12>&nbsp;<input type="submit" name="btnFind" value="Submit" class="btn btn-primary">
</form>
<{/if}>
<{if $is_pid_set}>
<h3>Status of <a href=problem.php?pid=<{ $pid }> ><{ $pid }> - <{ $pname }></a></h3>
<{else}>
<h3>General Status</h3>
<{/if}>
<div align="center">
<table class="table table-striped">
  <tr>
    <th width="100px">Submit ID</th>
    <th width="150px">Date</th>
    <th width="100px">Username</th>
    <th>Problem</th>
    <th width="180ox">Status</th>
    <th width="50px">CPU</th>
    <th width="50px">Source</th>
  </tr>

  <{section name=id loop=$rs }>
    <{if ($rs[id].color eq '"red"') }>
    <tr class="success">
    <{elseif ($rs[id].color eq '"blue"') }>
    <tr class="info">
    <{elseif ($rs[id].color eq '"green"') }>
    <tr class="error">
    <{else}>
    <tr class="warning">
    <{/if}>
        <td>
        <{if $rs[id].view_src}>
            <a href=submission_details.php?sid=<{ $rs[id].sid }> ><{ $rs[id].sid }></a>
        <{else}>
            <{ $rs[id].sid }>
        <{/if}>
        </td>
      <td><{ $rs[id].date }></td>
      <td><{ $rs[id].uid }></td>
      <td><a href=problem.php?pid=<{ $rs[id].pid }> ><{ $rs[id].pid }> - <{ $rs[id].pname }></a></td>

      <td><b><font color=<{ $rs[id].color }> >
        <{if ($rs[id].status eq 'Compile Error') and $is_login and ($is_admin or ($suid eq $rs[id].uid)) }>
          <a href=err_msg.php?sid=<{ $rs[id].sid }> ><font color="blue"><{ $rs[id].status }></font></a>
        <{else}>
          <{ $rs[id].status }>
        <{/if}>
      </font></b></td>

      <td>
        <{if $rs[id].cpu}>
          <{$rs[id].cpu}>
        <{else}>
          &nbsp;
        <{/if}>
      </td>

      <td>
        <{if $is_login and $is_admin }>
            <a href=testdata.php?src=<{ $rs[id].sid }>.<{ $rs[id].source }> ><{ $rs[id].sourceone }></a>
        <{else}>
            <{ $rs[id].sourceone }>
        <{/if}>
      </td>
    </tr>
  <{/section}>
</table>
</div>

<p style="text-align:center"> <{ $first_page }> <{ $prev_page }> <{ $next_page }> <{ $last_page }> </p>
