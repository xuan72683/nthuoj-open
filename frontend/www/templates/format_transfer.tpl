<h3>Format Transformation</h3>
<hr>
  <form name="myform">
    Times New Roman : <br>
    <textarea name="in1" cols=60 rows=20 id="text3" onChange="trans2();"></textarea>
    <textarea name="out1" cols=60 rows=20 id="text4"></textarea><br>
<hr>
    Courier New : <br>
    <textarea name="in1" cols=60 rows=20 id="text1" onChange="trans1();"></textarea>
    <textarea name="out1" cols=60 rows=20 id="text2"></textarea><br>
  </form>
<hr>



<script>
function trans1() {
  text1 = document.getElementById("text1");
  text2 = document.getElementById("text2");
  text2.value = '<p><span style="font-size: large; "><span style="font-family:'+" 'Courier New'; "+'">'
    + text1.value.replace(/\n/gi, "<br />") + "</p>";
}

function trans2() {
  text1 = document.getElementById("text3");
  text2 = document.getElementById("text4");
  text2.value = '<p><span style="font-size: large; "><span style="font-family:'+" 'Times New Roman'; "+'">'
    + text1.value.replace(/\n/gi, "<br />") + "</p>";
}
</script>
