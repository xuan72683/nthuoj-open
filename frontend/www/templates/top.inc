<?
//session_start();
?>

<head>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<LINK REL="SHORTCUT ICON" HREF="http://acm.cs.nthu.edu.tw/favicon.ico"> 
</head>

<meta http-equiv="Content-Type"
content="text/html; charset=utf-8" />
<title>NTHU Online Judge</title>
<link rel="stylesheet" type="text/css" href="./css/layout.css" />
<link rel="stylesheet" type="text/css" href="./css/color.css" />

<body>
<div id="perm-links">
<ul>
<li><a href="index.php">Home</a></li>
<?
	if(!isset($_SESSION['db_is_logged_in']) || !$_SESSION['db_is_logged_in'])
		echo '<li><a href="login.php">Login</a></li>';
	else
		echo '<li><a href="logout.php">Logout</a></li>';
?>
<?
	if(!isset($_SESSION['db_is_logged_in']) || !$_SESSION['db_is_logged_in'])
		echo '<li><a href="register.php">Register</a></li>';
	else
		echo '<li><a href="profile.php">Profile</a></li>'; 
?>
</ul>
</div>

<h1 id="top">National Tsing Hua University Online Judge</h1>

