<?php
/*************
 * user_lib.php
 * This provides some functions relative to user and user levels
 * **********/

function isUserExist($uid)
{
    $query = "SELECT count(1) FROM users WHERE id = '$uid'";
    $result = mysql_query($query) or die("Query failed".mysql_error());
    $row = mysql_fetch_assoc($result);
    $isExist = $row['count(1)'];
    return $isExist? true : false ;
}

/***************
given uid, check whether the uid is an Advance user, including admin, judge, coowner.
***************/
function isAdvanceUser($uid)
{
    $isAdmin = isAdmin($uid);
    $isJudge = isJudge($uid);
    $isCoowner = isCoowner($uid);
    if( $isAdmin || $isJudge || $isCoowner ) return true;
    else return false;
}

/***************
given uid, check whether the uid is an Admin .
In other words, checks whether the uid is in admin table.
***************/
function isAdmin($uid)
{
    $query = "SELECT count(1) FROM admin WHERE id = '$uid'";
    $result = mysql_query($query) or die("Query failed in isAdmin().".mysql_error());
    $row = mysql_fetch_assoc($result);
    $isAdmin = $row['count(1)'];
    return $isAdmin? true: false;
}

/***************
given uid, check whether the uid is a judge .
In other words, checks whether the uid is in judge table.
***************/
function isJudge($uid)
{ 
    $query = "SELECT count(1) FROM judge WHERE id = '$uid'";
    $result = mysql_query($query) or die("Query failed in isJudge().".mysql_error());
    $row = mysql_fetch_assoc($result);
    $isJudge = $row['count(1)'];

    return $isJudge? true: false;
}

/***************
given uid, check whether the uid is a coowner .
In other words, checks whether the uid is in coowner table.
***************/
function isCoowner($uid)
{
    $query = "SELECT count(1) FROM coowner WHERE id = '$uid'";
    $result = mysql_query($query) or die("Query failed in isCoowner().".mysql_error());
    $row = mysql_fetch_assoc($result);
    $isCoowner = $row['count(1)'];
    return $isCoowner ? true: false;
}

/*************
 Givem uid1, uid2. Compare the both in there user level.
 if uid1 has higher level, return 1
 else if uid1 and uid2 has same level, return 0
 else uid1 is lower than uid2, return -1
 * ***********/
function userLevelCmp($uid1, $uid2)
{
    $level1 = getUserLevelInNum($uid1);
    $level2 = getUserLevelInNum($uid2);
    $levelDelta =  $level1 - $level2;
    if( $levelDelta > 0) return 1;
    else if( $levelDelta == 0) return 0;
    else return -1;
}

/*****
 Given uid, return the user level (highest one) in number represent.
 Define Admin level as 4, Judge as 3, coowner as 2, normal user as 1
 * *****/
function getUserLevelInNum($uid)
{
    if( isAdmin($uid) ) return 4;
    else if ( isJudge($uid) ) return 3;
    else if( isCoowner($uid) ) return 2;
    else return 1;
}
?>