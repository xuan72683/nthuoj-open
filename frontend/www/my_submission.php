<?php
/********************************************************
my_submission.php
This php checks the session to get tge user's uid.
And would go to 'status.php' with GET parameter of the uid.
In the status page with the 'uid' GET parameter, specific user's submissions would render.
********************************************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    if(!check_login()) {
		header('Location: login.php');
		exit;
	}
	$message="";
    $tpl = new Handler("My Submission", "my_submission.tpl");

    if(!check_login())
        $current_user="";
	else {
		$current_user = $_SESSION['uid'];
        $tpl->assign("suid", $current_user);
    }
	$rowsPerPage = 20;
	
	echo "<script lanuage='javascript'>location.href='status.php?uid=$current_user'</script>";
?>
