<?php
/***************************************
contest_archive.php
This renders a page that shows all history contests.
No parameter is required.
****************************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

	$errors = array();
	$message = '';

	if(isset($_POST['submit'])) {
        $con = get_database_object();

		if(!check_pname($_POST['cname']))
			$message = 'Contest name must contain 1-64 characters.';
		else {
			$cname = htmlspecialchars($_POST['cname'],ENT_QUOTES);
			$query = "INSERT INTO contest (cname) VALUES ('".$cname."')";
			mysql_query($query) or die('query failed'.mysql_error());
		}
		mysql_close($con);
	}
    
    $tpl = new Handler("Contest Archive", "contest_archive.tpl");

    $con = get_database_object();	
	$tmp = get_latest_contest($con);

	$query = "SELECT cid, cname, start_time, end_time, result 
              FROM contest 
              ORDER BY start_time DESC";
	$result = mysql_query($query) or die("Query failed".mysql_error());
	
    $rs = array();
    while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
		if(time() < strtotime($row['start_time']))	continue;
        if($row['cid'] == 1)    continue;
        array_push($rs, $row);
	}
    //$rs_time_reverse = array_reverse($rs);
    $tpl->assign("contest", $rs);
	mysql_close($con);

    $tpl->display("base.html");
?>
