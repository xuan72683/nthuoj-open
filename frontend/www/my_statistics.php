<?php
	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    if( !check_login() ) {
		header('Location: login.php');
		exit;
	}
	$ac = "Accepted";
	$ce = "Compile Error";
	$je = "Judge Error";
	$pe = "Presentation error";
	$re = "Runtime Error";
	$re2  = "Runtime Error(SIGSEGV)";
	$tle = "Time limit exceeded";
	$wa = "Wrong Answer";
	
	$message="";
	header('Refresh: 60');
	
    $tpl = new Handler("My Statistics", "my_statistics.tpl");
    $con = get_database_object();

    if( !check_login() )
        $current_user="";
    else
        $current_user=$_SESSION['uid'];
    
    $query = "SELECT verdict as status, count(verdict) as cnt 
              FROM submission_result_detail as SMRD , submissions as SM 
              WHERE SM.uid = '$current_user' and SMRD.sid = SM.sid
              GROUP BY status";
    $query = "SELECT DISTINCT(verdict) as status, count(verdict) as cnt FROM submission_result_detail where sid in (select distinct(sid) from submissions where uid = '$current_user') GROUP BY verdict";
  
  $result = mysql_query($query) or die("Query failed1: ".mysql_error());
    
    $total = 0;
    $rs1 = array();
	$tmpi;
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
       if(strpos($row['status'],'/')){
			unset($row['status']);
	   }
   //  echo $row['status']."<br />";
		array_push($rs1, $row);
        $total += $row['cnt'];
    }
    $tpl->assign("total", $total);

	/*$query = "SELECT distinct SM.pid, P.pname 
              FROM submissions as SM, problems as P 
              WHERE SM.uid = '".$current_user."' 
                AND (SM.status='Accepted') 
                AND SM.pid = P.pid 
              ORDER BY SM.pid ";
*/$query = "SELECT distinct SM.pid, P.pname 
              FROM submissions as SM, problems as P , submission_result_detail as SMRD 
              WHERE SM.uid = '".$current_user."' 
                AND SM.sid = SMRD.sid
                AND (SMRD.verdict='Accepted') 
                AND SM.pid = P.pid 
              ORDER BY SM.pid ";
	$result = mysql_query($query) or die("Query failed1: ".mysql_error());

    $rs_prob = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        array_push($rs_prob, $row);
	mysql_close($con);

    $tpl->assign("rs1", $rs1);
    $tpl->assign("rs_prob", $rs_prob);
    $tpl->assign("msg", $message);

    $tpl->display("base.html");
?>
