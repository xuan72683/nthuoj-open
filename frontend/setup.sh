echo "Creating Database">&2
echo "Please Enter your Mysql Password">&2
mysql -u root -p < createTableSqls.sql
echo "Database Created , default admin username/password is boss/boss ">&2
echo "Changing the Permission of Files">&2
chown  -R www-data:www-data *
mkdir nthuoj
mkdir nthuoj/source
chown -R mysql:mysql /var/lib/mysql/*
chmod -R 755 /var/lib/mysql
chown -R www-data:www-data nthuoj
