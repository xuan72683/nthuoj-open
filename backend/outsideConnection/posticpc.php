<?php
    include_once("monitor_lib.php");

    $slp_ln = 3;
    while (1) {
        sleep($slp_ln);
        if (login_icpc() === false) {
            $slp_ln += 3;
            continue;
        }
        else
            $slp_ln = 3;

        if (is_null($row = fetch_unjudged()))
            continue;
        
		$sid = $row["sid"];
        $icpc_pid = $row["realid"];
        $lang = $row["source"];
        $code_path = "/var/nthuoj/source/$sid.$lang";

        if (!file_exists($code_path) || !is_file($code_path)) {
            print_err_msg("error opening file $code_path");
            update_status($sid, "Judge Error", "sid");
            continue;
        }

        update_status($sid, "Sending to Judge", "sid");
        $code = file_get_contents($code_path);
        $icpc_sid = post_icpc($icpc_pid, $lang, $code);
        if ($icpc_sid !== false) {
            add_mapping($sid, $icpc_sid);
            update_status($icpc_sid, "ICPC Judging", "icpc_sid");
        }
    }

    exit(0);
?>

