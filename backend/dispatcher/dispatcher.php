<?php
/*************************
dispatcher.php
This process continuously checks whether there exist unjudged submissions.
If there's unjudged submissions, start 'submit_curl.php' process to send it to a available judge VM.
**************************/

session_start();

require_once("lib/database_tools.php");
require_once("dispatcherFunction.php");


writeLog("================================");
writeLog("================================");
$cmd = "chmod 666 /var/nthuoj/log/dispatcher.log";
shell_exec($cmd);
$machine = getMachine();
if($machine!=null)
	initMachine($machine);
else
	writeLog("get machine file error");
print_r($machine);

$i=0;
echo "submit_test start<br>";
writeLog("dispatcher start");
while(1)
{
	/*sleep(10);*/
	$con = get_database_object();
    $sidQuery = "SELECT * FROM submissions WHERE status = 'Being Judged' ORDER BY sid ASC LIMIT 100";
    $sidRs = mysql_query($sidQuery) or die(mysql_error());
	
	echo "sidRsNum".mysql_num_rows($sidRs)."\n";
	while( $sidRow = mysql_fetch_array($sidRs) ) {
		$sid = $sidRow['sid'];	
		$pid = $sidRow['pid'];
		$problemQuery = "SELECT * FROM problems where pid = '$pid'";
		$problemRs = mysql_query($problemQuery);
		$problemRow = mysql_fetch_array($problemRs);
		$tid = $problemRow['tid'];
		$codeLanType = $sidRow['source'];
		//getJudgeInfo($pid, $sid);
		
		if($tid == 0)
		{	
			$findMachine = 0;
			echo "before findmachine";
			writeLog("before find machine");
			$destMachine = null;
			while($destMachine==null)
			{
				$destMachine = getIdleMachine($machine);
				sleep(1);
			}

			if($destMachine != null)
			{
				echo "destMachineName=".$destMachine['machineName']."   destMachineIP=".$destMachine['machineIP']."\n";
				writeLog("submission ".$sid." use ".$destMachine['machineName'].":".$destMachine['machineIP']."to judge");
			}
			else
			{
				echo "destMachine=null!!\n";
				writeLog("find machine error");
			}
		
			$con = get_database_object();
			$updQuery = "UPDATE submissions SET status = 'Judging by ".$destMachine['machineName']."' WHERE sid = ".$sid;
			echo $updQuery."   ".$sid."\n";
			mysql_query($updQuery) or die(mysql_error());
			$judgeIP = $destMachine['machineIP'];
			$judgePage = "interface.php";
			$judgeURL = $judgeIP."/".$judgePage;
			echo "judgeURL=".$judgeURL."\n";	
		
			echo "send!\n";

			writeLog("send info to judge ".$destMachine['machineName'].":".$destMachine['machineIP']);
			$arg = $sid." ".$pid." ".$codeLanType." ".$destMachine['machineName']." ".$judgeURL;
			$handle = popen("/usr/bin/php submit_curl.php ".$arg." & >> output.html", "w");
			pclose($handle);
			echo "after popen\n";
		}
		else
		{
			$con = get_database_object();
            $updQuery = "UPDATE submissions SET status = 'Judging by other Judge' WHERE sid = ".$sid;
            echo $updQuery."   ".$sid."\n";
            mysql_query($updQuery) or die(mysql_error());
			
			writeLog("send info to other judge");
			$arg = $codeLanType." ".$pid." ".$tid." ".$sid;
			if(file_exists("/var/nthuoj/outsideConnection/sendToOtherJudge.sh"))
			{
				$handle = popen("/var/nthuoj/outsideConnection/sendToOtherJudge.sh ".$arg." & >> output.html", "w");
				pclose($handle);
			}
			else
				writeLog("send info to other judge error(no judge)");

		}
	}

	sleep(10);
	$i++;
}
echo "submit_test finished\n";
writeLog("dispatcher finished");
?>
